package polymorphism;

import java.io.*;
import java.util.*;


class Main {


	public static void main(String[] args) {

		//create ArrrayList of type String to store fruits 
		ArrayList<String>fruits = new ArrayList<>();



		// create object of FruitChecker class
		FruitChecker fruitChecker = new FruitChecker();

		// inside try catch block call checkFruit method to catch exception if any
		Scanner sc= new Scanner(System.in);
		String fruit1=sc.nextLine();
		String fruit2=sc.nextLine();
		String fruit3=sc.nextLine();

		fruitChecker.checkFruit(fruit1, fruits); 
		fruitChecker.checkFruit(fruit2,fruits);
		fruitChecker.checkFruit(fruit3, fruits);

	}
}




class FruitChecker {

	// create a method checkFruit to check if the fruit is already present or not
	// pass Fruit to be added as a string and list of fruits
	// method throws CustomException
	public void checkFruit(String fruit, ArrayList<String> fruits)  {

		// throw exception if fruit already present in ArrayList
		if(fruits.contains(fruit)) {
			System.out.println("duplicate fruits");
		}
		else {
			// insert fruit to ArrayList
			fruits.add(fruit);
			System.out.println(fruit);
		}
	}
}



