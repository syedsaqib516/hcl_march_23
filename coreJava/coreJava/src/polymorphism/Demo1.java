package polymorphism;

class Parent
{
	final double PI=3.14;
	
	 public void marry()
	{
		
		System.out.println("marry subulakshmi");
		
	}
}
class Child extends Parent
{
	@Override
	public void marry()
	{
		System.out.println("marry Sai Pallavi");		
	}
}
public class Demo1 
{
	public static void main(String[] args) {
		Parent p = new Parent();
		p.marry();
		Child c = new Child();
		c.marry();
	}
}
