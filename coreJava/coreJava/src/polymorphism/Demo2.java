package polymorphism;

abstract class Calculator
{
 abstract void add();
}
class ScientificCalculator extends Calculator
{

	@Override
	void add() {
		System.out.println("scientific add impl");
		
	}
	void scientificFunc()
	{
		System.out.println("scientific funct");
	}
	
}
class SimpleCalculator extends Calculator
{

	@Override
	void add() {
		System.out.println("simple add impl");		
	}
	
	void simpleFunc()
	{
		System.out.println("simple funct");
	}
	
}
public class Demo2 
{
	public static void main(String[] args) 
	{
		Calculator calc = new SimpleCalculator();// upcasting
		calc.add();
		((SimpleCalculator)(calc)).simpleFunc();// downcastin
		           calc = new ScientificCalculator();// upcasting
		calc.add();
		((ScientificCalculator)(calc)).scientificFunc();// down cast
		
		
		
	}

}
