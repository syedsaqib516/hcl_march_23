package jdbcDemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Demo1 
{
	public static void main(String[] args) throws ClassNotFoundException, SQLException
	{
		Class.forName("com.mysql.cj.jdbc.Driver");// loading driver on ram to start using (same like creating object but without new kw)
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl","root","root");
		Statement stmt = con.createStatement();
		ResultSet rs=stmt.executeQuery("SELECT * FROM hcl_employees");
		// b=stmt.executeUpdate("insert into hcl_employees values (107,'syed saqib',200000,'8939387388')");
	    int b=stmt.executeUpdate("delete from hcl_employees where emp_id=107");
		if(b!=0)
		{
			System.out.println(b+" record(s) effeted");
		}
		else 
		{
			System.out.println("something went wrong :(");
		}

//		System.err.println("emp_id emp_name            emp_sal       emp_phone");
//		System.err.println("----------------------------------------------------");
//		//		while(rs.next())
//		//		{
//		//			System.out.println(rs.getInt(1)+"     "+rs.getString(2)+"             "+rs.getInt(3)+"      "+rs.getString(4));
//		//		}
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		List<Employee> l= new ArrayList<Employee>();
//		while(rs.next())
//		{
//			Employee emp = new Employee();
//			emp.setEmpId(rs.getInt(1));
//			emp.setEmpName(rs.getString(2));
//			emp.setEmpSal(rs.getInt(3));
//			emp.setEmpPhone(rs.getString(4));
//			System.out.println(emp);
//			l.add(emp);
//		}
//
//		List<Employee> salGreater10000=l.stream().filter(e->e.getEmpSal()>10000).collect(Collectors.toList());
//		List<Employee> bonus10k=l.stream().map(e->{ e.setEmpSal(e.getEmpSal()+10000); return e;}).collect(Collectors.toList());
//
//
//		for(Employee e: bonus10k)
//		{
//			System.out.println(e);
//		}

	}

}

//bolean execute
//Resultset executeQuery
//int executeUpdate
