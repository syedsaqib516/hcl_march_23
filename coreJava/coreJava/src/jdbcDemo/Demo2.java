package jdbcDemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class Demo2 {
	public static void main(String[] args) throws Exception 
	{
		Class.forName("com.mysql.cj.jdbc.Driver");// loading driver on ram to start using (same like creating object but without new kw)
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl","root","root");
		Statement stmt = con.createStatement();
		ResultSet rs=stmt.executeQuery("SELECT * FROM hcl_employees");
		 
		
	// b=stmt.executeUpdate("insert into hcl_employees values (107,'syed saqib',200000,'8939387388')");
	  //  int b=stmt.executeUpdate("delete from hcl_employees where emp_id=107");
	 
	    PreparedStatement pstmt=  con.prepareStatement("insert into hcl_employees values (?,?,?,?)");
	    pstmt.setInt(1, 108);
	    pstmt.setString(2,"syed ");
	    pstmt.setInt(3,300000);
	    pstmt.setString(4,"908989000");
	   boolean b= pstmt.execute();
		if(b==false)
		{
			System.out.println(b+" record(s) effeted");
		}
		else 
		{
			System.out.println("something went wrong :(");
		}
		
	}

}
