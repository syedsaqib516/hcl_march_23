package exceptionHandling;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Demo1 
{
	public static void main(String[] args) 
	{
		System.out.println("main started");
		int denomenator=-1;
		int numerator=-1;

		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("enter numerator");// code which is anticipated to cause an execption
			numerator= sc.nextInt();
			sc.nextLine();	// new InputMismatchException();		
			denomenator= sc.nextInt();
			sc.nextLine();


			System.out.println(numerator/denomenator);// new ArithmeticException();
		}
		
		catch(InputMismatchException ae)
		{
			System.out.println("please integeres only");// specific catch block
		}
		catch(ArithmeticException ae)
		{
			System.out.println(" please enter non zero demonenator");
		}
		catch(ArrayIndexOutOfBoundsException ae)
		{
			System.out.println(" please enter non zero demonenator");
		}
		catch(RuntimeException ae)
		{
			System.out.println(" something went wrong please try later");// generic catch block
		}
		catch(Exception ae)
		{
			System.out.println(" something went wrong please try later");// generic catch block
		}
		
		//		 exception Object is created , thrown to RTS 
		//		  RTS will check if theres any code written by programmer to handle the exception
		//		 it will give it to the program itself to handle it
		//		 else it will give to DefaultExceptionHandler(JVM) to handle it
		//		 JVM will abnormally terminate the program
		//		 display the execption message on the console.

		System.out.println("main ended");

	}

}
