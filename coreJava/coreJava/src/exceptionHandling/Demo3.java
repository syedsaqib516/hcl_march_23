package exceptionHandling;

public class Demo3 {
	public static void main(String[] args) {

		System.out.println(10/0);// finally is not executed

		try {
			try {

			} catch (Exception e2) {
				// TODO: handle exception
			}
			finally
			{}
			System.out.println("dummy try");

		} catch (Exception e) {
			try {

			} catch (Exception e2) {
				// TODO: handle exception
			}
			finally
			{}
			System.out.println(" dummy catch");

		}
		finally
		{
			try {

			} catch (Exception e2) {
				// TODO: handle exception
			}
			finally
			{}
			System.out.println("finally executed");
		}
	}
}
