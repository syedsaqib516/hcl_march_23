package exceptionHandling;

public class BankTransaction 
{
	public void validateBalance(double amtBal,double amtWithDraw)
	{
		System.out.println("validate Balance method start");
		if(amtBal>amtWithDraw)
		{
			System.out.println("withdrawl successful");
			amtBal=amtBal-amtWithDraw;
		}
		else
			try {
				throw new InsufficientBalanceException("insufficient funds");
			} catch (InsufficientBalanceException e) {
				System.out.println(e.getMessage());
			}
		System.out.println(" available balance ="+amtBal);
		System.out.println("validate Balance method end");
	}

}
