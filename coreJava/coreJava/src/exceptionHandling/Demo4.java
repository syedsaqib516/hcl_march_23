package exceptionHandling;

import java.io.IOException;
import java.sql.SQLException;

public class Demo4 
{
	public static void main(String[] args) throws IOException, SQLException
	{
		System.out.println("main start");
		m1();
		System.out.println("main end");

	}

	private static void m1() throws IOException, SQLException {
		System.out.println("m1 start");
		m2();
		System.out.println("m1 end");

	}

	private static void m2() throws IOException, SQLException {
		System.out.println("m2 start");
		m3();
		System.out.println("m2 end");

	}

	private static void m3() throws IOException,SQLException{
		System.out.println("m3 start");
	    throw new IOException();

	}

}
