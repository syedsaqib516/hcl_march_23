package streamsAPI;

@FunctionalInterface
interface Itr1
{
	void m1(int x);
}

class Demo
{
	public int m2(int a)
	{
		System.out.println("logic that i need");
		return 0;
	}
}
public class MethodRefDemo {
	public static void main(String[] args) {
		Demo d = new Demo();
		Itr1 i = d::m2;// method ref operator ::
		i.m1(10);
	}
}
