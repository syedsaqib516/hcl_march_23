package streamsAPI;

import java.util.stream.Stream;

public class Demo5
{
	public static void main(String[] args) 
	{
		Stream<Integer> s=Stream.of(1,2,3,4567,7,89,9,990);
		s.forEach(System.out::println);
		
		Integer[] i = {1,2,3,4567,7,89,9,990};
		Stream s1=Stream.of(i);
	}
}
