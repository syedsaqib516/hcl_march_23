package streamsAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Demo1 
{
	public static void main(String[] args)
	{
		ArrayList<Integer> l = new ArrayList<Integer>();
		l.add(10);
		l.add(0);
		l.add(20);
		l.add(16);
		l.add(25);
		l.add(5);
		l.add(9);
		System.out.println(l);
		// java 1.7  processing the collection without streams concept
		ArrayList<Integer> evenList = new ArrayList<Integer>();
		for(Integer I1:l)
		{
			if(I1%2==0)
			{
				evenList.add(I1);
			}
		}
		System.out.println(evenList);
		
		// java 1.8  processing the collection with streams concept
		
		
		
		
		
		
		List<Integer> eveList=l.stream().filter(i-> i%2==0).collect(Collectors.toList());
		System.out.println(eveList);
		
		
		
	}

}
