package streamsAPI;

@FunctionalInterface
interface Itr2
{
	Test m1(String x);
}
class Test
{
	Test()
	{
		System.out.println("no arg constructor");
	}
	Test(int x)
	{
		System.out.println("int arg constructor");
	}
	Test(String s)
	{
		System.out.println("String arg constructor");
	}
}

public class ConstructorRefDemo 
{
	public static void main(String[] args) {
		Itr2 i = Test::new;
		i.m1("abc");
	}
}
