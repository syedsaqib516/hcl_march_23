package streamsAPI;

import java.util.ArrayList;
import java.util.function.Consumer;

public class Demo4 
{
	public static void main(String[] args)
	{
		ArrayList<Integer> marks = new ArrayList<Integer>();
		marks.add(10);
		marks.add(0);
		marks.add(35);
		marks.add(20);
		marks.add(38);
		marks.add(70);
		marks.add(90);
		System.out.println(marks);

		marks.stream().forEach(System.out::println);


		Consumer<Integer> c= i->{
			System.out.println(" the square of the marks "+i+" is = "+(i*i));
		};

		marks.stream().forEach(c);
		marks.stream().forEach(i->{
			System.out.println(" the square of the marks "+i+" is = "+(i*i));
		});

		Object[] marksArr=marks.stream().toArray();
		for(Object obj1:marksArr)
		{
			System.out.println(obj1);
		}

		Integer[] arrMarks=  marks.stream().toArray(Integer[]::new);
		for(Integer i1:arrMarks)
		{
			System.out.println(i1);
		}


	}

}
