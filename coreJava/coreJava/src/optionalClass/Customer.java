package optionalClass;

import java.util.List;
import java.util.Optional;

public class Customer
{
	private Integer custId;
	private String custName;
	private String custEmail;
	List<String> phone;
	public Customer(Integer custId, String custName, String custEmail, List<String> phone) {
		super();
		this.custId = custId;
		this.custName = custName;
		this.custEmail = custEmail;
		this.phone = phone;
	}
	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public Optional<String> getCustName() {
		return Optional.ofNullable(custName);
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustEmail() {
		return custEmail;
	}
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	public List<String> getPhone() {
		return phone;
	}
	public void setPhone(List<String> phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "Customer [custId=" + custId + ", custName=" + custName + ", custEmail=" + custEmail + ", phone=" + phone
				+ "]";
	}
	

}
