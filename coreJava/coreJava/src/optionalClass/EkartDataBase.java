package optionalClass;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;



public class EkartDataBase
{

	public static List<Customer> getAll()
	{
		return Stream.of(
				new Customer(101,"saqib","saqib@gmail.com",Arrays.asList("9090909","9987767")),
				new Customer(102,"sai","sai@gmail.com",Arrays.asList("890900","1122267")),	
				new Customer(103,"arjun","arjun@gmail.com",Arrays.asList("90909099","9243767")),	
				new Customer(104,"rahul","rahul@gmail.com",Arrays.asList("9090909","95555567")),	
				new Customer(106,"satish","satish@gmail.com",Arrays.asList("909999990","9985557767")),	
				new Customer(107,"priya","priya@gmail.com",Arrays.asList("90909000009","9981227767")),	
				new Customer(108,"pramila","pramila@gmail.com",Arrays.asList("933090909","998711767"))

				).collect(Collectors.toList());
	}

}
