package optionalClass;

import java.io.IOException;
import java.util.logging.Level;

public class LogDemo 
{
	public static void main(String[] args) throws SecurityException, IOException 
	{

		Log myLog= new Log("logs.txt");

		myLog.logger.setLevel(Level.WARNING);
		boolean validCredentials=false;

		if(validCredentials) 
		{
			myLog.logger.info("loggin successful ,welcome to ekart");
		}
		else
		{
			myLog.logger.warning("invalid credentials try again");
		}


		boolean transactionSuccessful=false;


		if (transactionSuccessful && validCredentials)
		{
			myLog.logger.info("Order placed successfully");	
		}
		else if(!validCredentials)
		{
			myLog.logger.warning("please enter valid pic");
		}
		else
		{
			myLog.logger.severe("unable to connect to payment gateway please try later");
		}


		myLog.logger.info(" logout successful ,thank you for using ekart");

	}


}
