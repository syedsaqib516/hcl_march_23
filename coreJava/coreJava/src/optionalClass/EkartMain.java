package optionalClass;

import java.util.List;

public class EkartMain
{
	
	public static Customer getCustByMail(String email) throws CustomerNotFoundException
	{
		List<Customer> customers=EkartDataBase.getAll();
		return customers.stream()
				.filter(customer->customer.getCustEmail()
						.equalsIgnoreCase(email)).findFirst()
				             .orElseThrow(()->new CustomerNotFoundException("no customer with the given mail found"));
	}
	
	public static void main(String[] args) 
	{
	 
		
			Customer c=null;
			try {
				c = getCustByMail("saqib@gmail.com");
			} catch (CustomerNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}
		   System.out.println(c);
		
	}

}
