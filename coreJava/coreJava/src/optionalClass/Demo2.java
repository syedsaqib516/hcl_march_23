package optionalClass;

import java.util.Optional;

public class Demo2 
{
	public static void main(String[] args)
	{
		String email="Saqib@Gmail.Com";
		
		Optional<String>  eO = Optional.ofNullable(email);
		
		System.out.println(eO.map(String::toLowerCase).orElseGet(()->"default@gmail.com"));
		
		
		
	}

}
