package optionalClass;

import java.util.Optional;

public class Demo1
{
	public static void main(String[] args)
	{
		String email=null;

//		Optional emptyOp= Optional.empty();
//		System.out.println(emptyOp);

//		Optional<String> so=Optional.of(email);
//		System.out.println(so);
		
		Optional<String> son=Optional.ofNullable(email);
		System.out.println(son);
		//System.out.println(son.get());
		
          if(son.isPresent())
          {
        	  son.ifPresent(value->System.out.println(son.get()));
          }
          else
          {
        	  System.out.println("no value present");
          }
          
          son.ifPresent(value->System.out.println("value is present"));
          
//          System.out.println(son.get());
        
          
          System.out.println(son.orElse("default@gmail.com"));
          
          try {
			System.out.println(son.orElseThrow(()->new Exception("no mail found")));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
          
          System.out.println(son.orElseGet(()->"default mail is default@gmail.com"));
          
	}

}
