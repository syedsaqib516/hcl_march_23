package multithreading;

import java.util.Scanner;

public class Demo2 {
	public static void main(String[] args) throws InterruptedException {
		System.out.println("char printing activity started");
		for (int i = 65; i < 75; i++) 
		{
			System.out.println((char)i);
			Thread.sleep(1000);
		}
		System.out.println("char printing activity ended");
		System.out.println("number printing activity started");
		for (int i = 0; i < 10; i++) 
		{
			System.out.println(i);
			Thread.sleep(1000);
		}
		System.out.println("number printing activity ended");

		
		System.out.println("banking activity started");
		Scanner sc= new Scanner(System.in);
		System.out.println("enter bank acc number");
		int accNo = sc.nextInt();
		System.out.println("enter pin code");
		int pin =sc.nextInt();
		System.out.println("please collect your cash , thank");
		System.out.println("banking  activity ended");

	}
}
