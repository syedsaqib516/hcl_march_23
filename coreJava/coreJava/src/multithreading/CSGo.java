package multithreading;

public class CSGo extends Thread
{
	String weapon1= "glock";
	String weapon2="maverick";
	String weapon3= "grenade";
	
	public void run()
	{
		if(getName().equals("Nanu"))
		{
			try {
				nanuBashaAcc();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (getName().equals("Harshit"))
		{
			try {
				harshitAcc();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
	public void nanuBashaAcc() throws InterruptedException
	{
		synchronized(weapon1)
		{
			System.out.println("nanu has accquired "+weapon1);
			Thread.sleep(3000);
			synchronized(weapon2)
			{
				System.out.println("nanu has accquired "+weapon2);
				Thread.sleep(3000);
				synchronized(weapon3)
				{
					System.out.println("nanu has accquired "+weapon3);
					Thread.sleep(3000);
				}
			}
		}
		
	}
	public void harshitAcc() throws InterruptedException
	{
		synchronized(weapon3)
		{
			System.out.println("Harshit has accquired "+weapon3);
			Thread.sleep(3000);
			synchronized(weapon2)
			{
				System.out.println("Harshit has accquired "+weapon2);
				Thread.sleep(3000);
				synchronized(weapon1)
				{
					System.out.println("Harshit has accquired "+weapon1);
					Thread.sleep(3000);
				}
			}
		}
	}

}
