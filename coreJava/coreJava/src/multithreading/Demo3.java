package multithreading;

public class Demo3 
{
	public static void main(String[] args) 
	{
		NumPrint np = new NumPrint();
		CharPrint cp = new CharPrint();
		BankActivity ba= new BankActivity();
		Thread t1 = new Thread(np);
		Thread t2 = new Thread(cp);
		Thread t3 = new Thread(ba);
		t1.start();
		t2.start();
		t3.start();
		
//		t1.run();
//		t2.run();
//		t3.run();
		
		
	}

}
