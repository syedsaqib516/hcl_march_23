package multithreading;

public class MsWordMain 
{
	public static void main(String[] args) throws InterruptedException {
		MsWord t1 = new MsWord();
		MsWord t2 = new MsWord();
		MsWord t3 = new MsWord();
		t1.setName("type");
		t2.setName("spellCheck");
		t3.setName("autoSave");
		t2.setDaemon(true);
		t3.setDaemon(true);
		t1.setPriority(10);
		t2.setPriority(1);
		t3.setPriority(1);
		t1.start();
		
		t2.start();
		
		t3.start();
		
	}

}
