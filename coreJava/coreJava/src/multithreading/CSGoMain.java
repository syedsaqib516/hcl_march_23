package multithreading;

public class CSGoMain 
{
	public static void main(String[] args) {
		CSGo t1 = new CSGo();
		CSGo t2 = new CSGo();
		
		t1.setName("Nanu");
	
		t2.setName("Harshit");
		t1.start();
		t2.start();
		// Dead Lock.
	}

}
