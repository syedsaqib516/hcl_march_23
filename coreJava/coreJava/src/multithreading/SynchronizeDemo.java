package multithreading;

class Resource implements Runnable
{
	public synchronized void run()
	{
		try {
			System.out.println(Thread.currentThread().getName()+" got the ticket number 1");
			Thread.sleep(3000);
			System.out.println(Thread.currentThread().getName()+" is booking ticket 1");
			Thread.sleep(3000);
			System.out.println(Thread.currentThread().getName()+" got seat 1");			
		}
		catch(Exception e)
		{
			System.out.println("handled");
		}
	}
}
public class SynchronizeDemo
{
	public static void main(String[] args) {
		Resource r = new Resource();
		Thread t1= new Thread(r);
		Thread t2= new Thread(r);
		Thread t3= new Thread(r);
		t1.setName("thread 1");
		t2.setName("thread 2");
		t3.setName("thread 3");
		t1.start();
		t2.start();
		t3.start();

	}
}
