package classesObjectsAndStatic;

public class Student 
{
	int rollNo;
	String name;
	double marks;
	static String SCHOOL_NAME="Great Learning School";
	
	public Student(int rollNo, String name, double marks) {
		System.out.println("constructor executed");
		this.rollNo = rollNo;
		this.name = name;
		this.marks = marks;
	}
	
	{
		System.out.println("non static block executed");
	}
	
	static 
	{
		// executed right after class loading
		System.out.println("static block executed");
	}
	static void m1()
	{
		
		System.out.println("static method executed");
	}
	void m2()
	{
		System.out.println("non static method" +SCHOOL_NAME);
	}
	public static void main(String[] args) 
	{
		System.out.println("main start");
		Student.m1();
		System.out.println(Student.SCHOOL_NAME);
		Student s1 = new Student(1,"abc",99.99);
		s1.m2();
		System.out.println(s1.name);
		
		System.out.println("main end");
	}

}
