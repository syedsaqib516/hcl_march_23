package classesObjectsAndStatic;

public class Overloading 
{
	public static void main(String[] args) 
	{
	 System.out.println( add("syed "," saqib"));	
	}
	// method overloading - static/virtualcompileTime polymorphism
	static double add(int a , double d)
	{
		return a+d;
	}
	static double add(double a , int b)
	{
		return a+b;
	}
	static int add(int a,int b,int c)
	{
		return a+b+c;
	}
	static int add(int a , int b,int c,int d)
	{
		return a+b+c+d;
	}
	static String add(String s1,String s2)
	{
		return s1+s2;
	}


}
