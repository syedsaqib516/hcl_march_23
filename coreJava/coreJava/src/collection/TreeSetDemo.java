package collection;

import java.util.TreeSet;

import encapsulation.Employee;

public class TreeSetDemo
{
	public static void main(String[] args) 
	{
		TreeSet<Integer> ts = new TreeSet<Integer>(new MyComparator());
		ts.add(10);
		ts.add(5);
		ts.add(7);
		System.out.println(ts);
		
		TreeSet<Employee> t = new TreeSet<Employee>();
		t.add(new Employee(10,"a",122));
		t.add(new Employee(21,"c",112));
		t.add(new Employee(13,"b",123));
		System.out.println(t);
		TreeSet<Employee> t1 = new TreeSet<Employee>(new EmployeeIdComparator());
		t1.add(new Employee(10,"a",122));
		t1.add(new Employee(21,"c",112));
		t1.add(new Employee(13,"b",123));
		System.out.println(t1);
		
		
	}

}


