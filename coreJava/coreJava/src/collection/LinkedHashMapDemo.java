package collection;

import java.util.LinkedHashMap;

public class LinkedHashMapDemo
{
	public static void main(String[] args) {
		
		LinkedHashMap<Integer,String> hm = new LinkedHashMap<Integer,String>();
		hm.put(1,"abcd");
		hm.put(2,"bcde");
		hm.put(3,"fghi");
		hm.put(4,"jklm");
		hm.put(5,"nopq");
		hm.put(1, "zyx");
		hm.put(12,null);
		hm.put(11,"avv");
		System.out.println(hm);
		
		
	}

}
