package collection;

import java.util.Iterator;
import java.util.LinkedList;

public class LinkedListDemo
{
	public static void main(String[] args) {
		LinkedList al = new LinkedList();
		al.add(10);
		al.add(100.0);
		al.add(1000.5f);
		al.add("abcd");
		al.add('a');
		al.add(true);
		System.out.println(al);
		Iterator i=al.iterator();// universal cursor
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
		
		for(Object obj:al)// all collection
		{
			System.out.println(obj);
		}
		for (Iterator iterator = al.iterator(); iterator.hasNext();) {
			Object object = (Object) iterator.next();
			System.out.println(object);
			
		}
		for (int j = 0; j < al.size(); j++) // list only
		{
			System.out.println(al.get(j));			
		}		
	}

}
