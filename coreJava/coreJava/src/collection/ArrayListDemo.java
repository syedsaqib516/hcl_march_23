package collection;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListDemo 
{
	public static void main(String[] args) {
		ArrayList al = new ArrayList();
		al.add(10);
		al.add(100.0);
		al.add(1000.5f);
		al.add("abcd");
		al.add('a');
		al.add(true);
		al.add(0, "new");
		System.out.println(al);
		
		Iterator i=al.iterator();// universal cursor
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
		
		for(Object obj:al)// all collection
		{
			System.out.println(obj);
		}
		for (Iterator iterator = al.iterator(); iterator.hasNext();) {
			Object object = (Object) iterator.next();
			System.out.println(object);
			
		}
		for (int j = 0; j < al.size(); j++) // list only
		{
			System.out.println(al.get(j));			
		}		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
