package collection;

import java.util.ArrayList;
import java.util.Collections;

public class GenericsDemo1 
{
	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(10);
		al.add(100);
		al.add(1000);
		al.add(1);
		Collections.sort(al);
		System.out.println(al);
	}

}
