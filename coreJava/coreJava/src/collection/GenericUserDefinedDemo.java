package collection;

class Gen<T>
{
	T i;

	public T getI() {
		return i;
	}

	public void setI(T i) {
		this.i = i;
	}

	public Gen(T i) {
		super();
		this.i = i;
	}
	@Override
	public String toString() {
		return "Gen [i=" + i + "]";
	}

}

public class GenericUserDefinedDemo 
{
	public static void main(String[] args) {

		Gen<Integer> g1 = new Gen<Integer>(10);
		System.out.println(g1);
		Gen<String> g2 = new Gen<String>("10abcd");
		System.out.println(g2);
	}
}
