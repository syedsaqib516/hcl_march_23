package collection;

import java.util.TreeMap;

public class TreeMapDemo 
{
	public static void main(String[] args) {
		TreeMap<Integer,String> hm = new TreeMap<Integer,String>(new MyComparator());
		hm.put(1,"abcd");
		hm.put(2,"bcde");
		hm.put(3,"fghi");
		hm.put(4,"jklm");
		hm.put(5,"nopq");
		hm.put(1, "zyx");
		hm.put(12,null);
		hm.put(11,"avv");
		System.out.println(hm);
		
		
	}

}
