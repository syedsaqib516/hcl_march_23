package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;
import java.util.Vector;

public class VectorStackDemo 
{
	public static void main(String[] args) {

		Vector v = new Vector();// its a same as arraylist except that its synchronized
		v.addElement(1);
		System.out.println(v);
				
		Stack s = new Stack();// LIFO mechanism
//		s.add(1);
//		s.add(2);
//		s.add(3);
		s.push(1);
		s.push(2);
		s.push(3);
		//s.pop();
		
		System.out.println(s.peek());
		System.out.println(s.search(3));
		System.out.println(s.search(1));
		
		
		
		
	}
}
