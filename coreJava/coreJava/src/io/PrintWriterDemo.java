package io;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class PrintWriterDemo 
{
	public static void main(String[] args) throws IOException {
		MotorCycle m = new MotorCycle("Yamaha","R1",999,2500000.0);
		PrintWriter pw= new PrintWriter(new FileWriter("demo.txt"));
		pw.println(100);
		pw.println("Oh hello there");
		pw.println("hi");
		pw.print(m);
		pw.flush();
		pw.close();
	}

}
