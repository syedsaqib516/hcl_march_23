package io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedWriterDemo 
{
	public static void main(String[] args) throws IOException 
	{
		FileWriter fw = new FileWriter("demo.txt");
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(100);
		bw.newLine();
		char ch[]= {'a','b','c','d'};
		bw.write(ch);
		bw.newLine();
		bw.write("hello world how are you");
		bw.flush();
		bw.close();//fw will get closed automatically
		
		
	}

}
