package io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FisDemo1 {
	public static void main(String[] args) throws IOException {
          FileInputStream fis = new FileInputStream("Input.mp4");
          
          int size=fis.available();
          
          byte [] b = new byte[size];
          
         fis.read(b);
         
         //System.out.println(new String(b));\
         File img = new File("output.mp4");
         img.createNewFile();
         
         FileOutputStream fos = new FileOutputStream(img);
         
         fos.write(b);
         
        
         
         fis.close();
         fos.close();
         
          
          
	}
}
