package io;

import java.io.Serializable;

public class Account implements Serializable
{
	private static long serialVersionUID=9909090909999l;// secure hash algorithm
	long accNo;//   non static variable/ instance variable
	String accType;// //   non static variable/ instance variable
	String accPassword="abcd@123";//   non static variable/ instance variable
	transient String ifscCode= "ABC11990";//   non static variable/ instance variable

	static final String BANK_NAME="ABCD BANK"; // static variable/ Class variable

	public long getAccNo() {
		return accNo;
	}

	public void setAccNo(long accNo) {
		this.accNo = accNo;
	}

	public String getAccType() {
		return accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

	public String getAccPassword() {
		return accPassword;
	}

	public void setAccPassword(String accPassword) {
		this.accPassword = accPassword;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public static String getBankName() {
		return BANK_NAME;
	}

	public Account(long accNo, String accType, String accPassword, String ifscCode) {
		super();
		this.accNo = accNo;
		this.accType = accType;
		this.accPassword = accPassword;
		this.ifscCode = ifscCode;
	}

	@Override
	public String toString() {
		return "Accounts [accNo=" + accNo + ", accType=" + accType + ", accPassword=" + accPassword + ", ifscCode="
				+ ifscCode + "]";
	}

	
	
	
	
}
