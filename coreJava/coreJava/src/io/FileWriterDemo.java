package io;

import java.io.FileWriter;
import java.io.IOException;

public class FileWriterDemo 
{
	public static void main(String[] args) throws IOException  
	{
		
		try(FileWriter fw=new FileWriter("demo.txt"))
		{
			fw.write("monkey\nKing");
			fw.write("\n");
			char[] ch= {'s','i','m','b','a'};
		    fw.write(ch);
		    fw.write("\n");
		    fw.write("Hi World");
		    fw.flush();
		} 
		
			//fw.write(100);//d
		
	  
	    
	}

}
