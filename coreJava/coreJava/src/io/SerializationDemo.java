package io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializationDemo 
{
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		
		//Serialization
		Account acc= new Account(9090530015l,"salary","8090","SIBL0090");
		FileOutputStream fos = new FileOutputStream("accounts.txt");
		ObjectOutputStream oos= new ObjectOutputStream(fos);
		oos.writeObject(acc);
		
		//Deserialization
		FileInputStream fis = new FileInputStream("accounts.txt");
		ObjectInputStream ois= new ObjectInputStream(fis);
		Account accDes=(Account) ois.readObject();
		System.out.println(accDes);
	}

}
//Serialization in Java is a mechanism of writing the state of an object into a byte-stream. It is mainly used in Hibernate, RMI, JPA, EJB and JMS technologies.
//
//The reverse operation of serialization is called deserialization where byte-stream is converted into an object. The serialization and deserialization process is platform-independent, it means you can serialize an object on one platform and deserialize it on a different platform.
//
//For serializing the object, we call the writeObject() method of ObjectOutputStream class, and for deserialization we call the readObject() method of ObjectInputStream class.
//
//We must have to implement the Serializable interface for serializing the object.

