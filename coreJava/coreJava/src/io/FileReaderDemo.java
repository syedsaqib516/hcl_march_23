package io;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileReaderDemo 
{
	public static void main(String[] args) throws IOException {
		
//		File f= new File("demo.txt");
//		FileReader fr= new FileReader(f);
//	    char[] ch=new char[(int)f.length()];
//	    fr.read(ch);
//		for (char c : ch) 
//		{
//          System.out.print(c);			
//		}
					
		
		FileReader fr= new FileReader("demo.txt");
		int i=fr.read();
		while(i!=-1)
		{
			System.out.print((char)i);
			i=  fr.read();
		}
		
	}

}
