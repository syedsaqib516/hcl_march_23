package io;

import java.io.FileWriter;
import java.io.IOException;

public class FRWDemo 
{
	public static void main(String[] args) throws IOException {
		
		FileWriter fw = new FileWriter("input1.txt");
		
		String data = " hello world, welcome to java";
		
		char[] ch= data.toCharArray();
		
		fw.write(ch);
		fw.flush();
	}

}
