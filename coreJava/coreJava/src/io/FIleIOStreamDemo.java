package io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FIleIOStreamDemo 
{
	public static void main(String[] args) throws IOException 
	{
		File in =new File("input.txt");
		File out =new File("output.txt");
	//	in.createNewFile();
	//	out.createNewFile();
		FileInputStream fis=new FileInputStream(in);
		
		BufferedInputStream bis = new BufferedInputStream(fis);
		FileOutputStream fos=new FileOutputStream(out);	
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		int temp;
		
			while ((temp=bis.read())!=-1)
			{
				bos.write(temp);
				
			}
		  bos.flush();
		  bis.close();
		  bos.close();
				
	}
}
