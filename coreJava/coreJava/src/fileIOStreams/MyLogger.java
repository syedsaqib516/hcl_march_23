package fileIOStreams;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MyLogger 
{
	static Logger logger=Logger.getLogger("myLog");
	
	public static void main(String[] args) throws SecurityException, IOException {
		
		try {
			FileHandler fh = new FileHandler("logs.txt");
			logger.addHandler(fh);
			
			SimpleFormatter sf = new SimpleFormatter();
			fh.setFormatter(sf);
		//	System.out.println(10/0);	
		logger.info("set up done");
		logger.warning("warning");
		logger.severe("severe");
			//logger.finest("success");
		} catch (Exception e) {
			logger.log(Level.WARNING,"logger set up failed :",e);
			logger.log(Level.SEVERE,"logger set up failed :",e);
			logger.log(Level.INFO,"logger set up failed :",e);
		}
	}
}