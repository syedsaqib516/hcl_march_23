package lambdaExpressions;

import java.time.LocalDate;
import java.util.Date;
import java.util.function.Consumer;

public class ConsumerDemo 
{
	public static void main(String[] args) {
		Consumer<LocalDate> c = d -> {
			System.out.println(d.getDayOfMonth());
			System.out.println(d.getMonth());
			System.out.println(d.getYear());	
		};
		c.accept(LocalDate.now());
	}
	

}
