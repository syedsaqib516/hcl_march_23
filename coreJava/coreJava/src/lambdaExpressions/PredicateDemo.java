package lambdaExpressions;

import java.util.function.Predicate;

import encapsulation.Employee;

	class Demo implements Predicate<Integer>
	{
	
		@Override
		public boolean test(Integer I) {
			if(I%2==0)
				return true;
			else
			return false;
		}	
	}

public class PredicateDemo 
{
	public static void main(String[] args) 
	{
	  Predicate<Integer> p = I -> I%2==0;
		
//	  System.out.println(p.test(10));
//	  System.out.println(p.test(5));
//	  System.out.println(p.test(0));
	  
	  Predicate<Employee> p1 = e -> e.getEmpSal()>1000;
		
	 // System.out.println(p1.test(new Employee(1,"abc",2000)));
	  
	  Predicate<String> p2 = s -> s.length()>=5;
		
	 // System.out.println(p2.test("saqi"));
	  
	  String[] names= {"abcd","efghi","jkl","mnopq","rst","uvwxyz"};
	  
	  Predicate<String> oddNameLen = s-> s.length()%2!=0;
	  Predicate<String> lenGreaterThan3 = s-> s.length()>3;
	  
	  for(String name:names)
	  {
		  if(oddNameLen.negate().and(lenGreaterThan3).or(oddNameLen).test(name))
			  System.out.println(name);
	  }
	  
	
	}

}
