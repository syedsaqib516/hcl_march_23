package lambdaExpressions;

import java.util.function.Function;

public class FunctionDemo
{
	public static void main(String[] args) 
	{
		Function<String,Integer> f=s -> s.length();
		
		Function<Integer,Integer> square= i->i*i ;
		
		System.out.println(f.apply("saqib"));
		System.out.println(square.apply(5));
		
		Function<Integer,Integer> cubeIt  = i->i*i*i;
		Function<Integer,Integer> doubleIt = i->2*i;
		
		System.out.println(cubeIt.andThen(doubleIt).apply(2));// 16
		System.out.println(cubeIt.compose(doubleIt).apply(2));// 64
		System.out.println(doubleIt.andThen(cubeIt).apply(2));// 64
		
		
		
		
		
		
		
		
		
	}

}
