package inheritance;

class A
{
	public int x=10;

	static int y=20;

	static void m1()
	{
		System.out.println("static method");
	}
	void m2()
	{
		System.out.println("non static method"); 
	}
}
class B extends A
{

}
class C extends B
{
	
}
class D extends A
{
	
}
//
//class E extends A,B not allowed in java
//{
//	
//}

//class F extends G
//{
//	
//}
//class G extends F
//{
//	
//}
//class H extends H
//{
//	
//}


class X //extends Object
{
//  X()
//  {
//	  super();
//  }
  // + 9 object class methods
}

public class Demo1
{
	public static void main(String[] args) 
	{
        A a = new A();
        A.m1();
        a.m2();
        System.out.println(a.x);
        System.out.println(A.y);
        
        B b = new B();
        B.m1();
        b.m2();
        System.out.println(b.x);
        System.out.println(B.y);
        
        C c = new C();
        c.m1();
        c.m2();
        System.out.println(c.x);
        System.out.println(C.y);
        
        X x= new X();
      
        
       
       
       

	}

}
