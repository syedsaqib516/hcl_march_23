package wrapperClasses;

import encapsulation.Employee;

public class Demo1 
{
	public static void main(String[] args)
	{
		int num1= 10;
		Integer num2 = 20;
		
		//primitive to non primitive (Object) wrapping/boxing
		int x1= 10;
		Integer x2 =x1;// implict boxing
		Integer x3 =Integer.valueOf(x1);//explicit boxing
		
		
		//non primitive(object) to primitive  unwraping/unboxing
		Integer y1 = new Integer(100);
		Integer y2 = Integer.valueOf(1000);
		int y3= y1;// implicit unboxing
		int y4 = y2.intValue();// explicit unboxing
		
		String s1 ="123";
		
		int x=Integer.parseInt(s1);// string to int
		Integer y=Integer.parseInt(s1);
		
		String s2=y2.toString();// int to string
		
		System.out.println(x2==x1);
		
		
		Object[] obj = new Object[3];
		obj[0]=new Integer(10);
		obj[1]=new String("abc");
		obj[2]= new Employee();
		
	}

}
