package basics;

public class DataTypes 
{
	public static void main(String[] args)
	{
		int num1 = 10;
	    int num2 = 20;
	    int sum= num1+num2;
//	    System.out.println("the result is = "+sum);
//	    System.out.println(num1+num2);
//	    System.out.println(" the sum of "+num1+" and "+num2+" is= "+(num1+num2));
//	   
//	    System.out.println(--b);
	    short s= 1000;
	    long l = 1000000L;
	    float pi = 3.14f;
	    double d= 12299.9987;
	    
	    boolean flag = false;
	    String name= "Saqib";
	    
	    // implicit type conversion
	    byte b = -128;
	    int i = b;
	    System.out.println(i);
	    char c= 'a';
	    int ascii=c;
	    System.out.println(ascii);
	    
	    //explicit type conversion
	    double pie=3.14;
	    byte bpi=(byte)pie;
	    System.out.println(bpi);
	    
	    char newChar=(char)65;
	    System.out.println(newChar);		
	}

}
