package basics;

	interface InterfaceName
	{
		//interface should be PascalCase
	}
	class PascalCaseConvention$// class name follows PascalCase
	{
		int employeeId1;
		String camelCaseConvention; // variable names follow camel case
		
		double PI=3.14;  // constants must be all caps.
		double SPEED_OF_LIGHT= 300000000;
		double ACC_DUE_TO_GRAVITY=9.8;
		
		void methodNam1()
		{
			System.out.println("methods follow camel case");
		}
		
	
	}
