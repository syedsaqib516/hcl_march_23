package basics;

import java.util.Arrays;

public class Methods 
{
	public static void main(String[] args) 
	{
		System.out.println(" main start");		
		int a = 10;
		int b = 20;
		         add(a,b);
		int res= sub(b,a);
		System.out.println(b+" minus "+a+" is ="+res);	
		System.out.println("the prod of 10 20 30 is ="+mul(10,20,30));
		System.out.println("main end");	
		String[] names = {"xyz","abc","def"};
		String [] sortedNames= sortNames(names);		
		for(String name:sortedNames)
		{
			System.out.println(name);
		}	 
	}
	public static void add(int a, int b)
	{
		int sum = a+b;
		System.out.println(" the sum of "+a+" and "+b+" is ="+sum);
		
	}
	public static int sub(int a , int b)
	{
		int res= a-b;
		return res;
	}
	public static int mul(int a , int b, int c)
	{
		return a*b*c;
	}
	
	public static String[] sortNames(String[] names) 
	{
	     Arrays.sort(names);
	     return names;
	}
}
