package basics;

public class StringMethods 
{
	public static void main(String[] args) 
	{
		String s1= "abcd";
		String s2= "abcd";
		String s3 = new String("abcd");
		String s4 = new String("abcd");
		
		System.out.println(s1==s2);// == for objects will compare address
		System.out.println(s3==s4);
		
		
		
		System.out.println(System.identityHashCode(s3));
		System.out.println(System.identityHashCode(s1));
		
		System.out.println(s1.concat("efgh"));
		System.out.println(s1.toUpperCase());
		System.out.println(s1.contains("ab"));
		System.out.println(s1.equals(s2));
		System.out.println(s1.compareTo(s2));
		
		String name= " raja ram mohan roy ";
		System.out.println(name.contains("mohan"));
		
		System.out.println(name.substring(7));
		System.out.println(name.substring(7,12));
		
		System.out.println();
		
		System.out.println(1==1);
		System.out.println(s1==s2);
		
		System.out.println("abcd".equals(s1));
		System.out.println(s1.equals("abcd"));
		System.out.println("abcd".equals("abcd"));
		System.out.println(s1.equals(s2));
		System.out.println("ABC".compareTo("AB"));
		
		System.out.println(s1.length());
	    
		String[] names=name.split(" ");
		
		for(String n:names)
		{
			System.out.println(n);
		}
		
		System.out.println(name.replace('r', 'R'));
		System.out.println(name.stripLeading());
		System.out.println(name.stripTrailing());
		System.out.println(name.strip());
		
	}

}
