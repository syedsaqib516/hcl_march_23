package basics;
import java.util.Scanner;

public class ReadingUserInput 
{
	public static void main(String[] args) 
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter your name");
		String name= scan.next();
		             scan.nextLine();// to consume buffer value
		System.out.println("your name is "+name);
		
		System.out.println("please enter your full name");
		String fullName= scan.nextLine();
		System.out.println("your name is "+fullName);
		
		System.out.println("enter your age ");
		byte age=scan.nextByte();
		System.out.println("your age is "+age);
		
		System.out.println("enter your gender M or F");
		char gender =scan.next().charAt(1);
		System.out.println("your gender is "+gender);
		
		scan.nextShort();
		scan.nextInt();
		scan.nextFloat();
		scan.nextDouble();		
	}

}
