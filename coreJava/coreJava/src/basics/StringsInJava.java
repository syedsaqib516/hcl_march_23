package basics;

public class StringsInJava 
{ 
	public static void main(String[] args)
	{ //immutable strings
		
		String name = "Syed Saqib";
		String gender = new String("male");
		
		gender.concat("-female");
		
		System.out.println(gender);
		
		
		
		
		
		
		
		
      // mutable
		StringBuffer skills = new StringBuffer("java,python");
		
		skills.append(",scala");
		
		System.out.println(skills);
		
		StringBuilder location= new StringBuilder("90 degree north 85 degree south");
	}

}
