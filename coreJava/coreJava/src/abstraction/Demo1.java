package abstraction;

abstract class A1
{
	public abstract void m1();
	void m2()
	{
		System.out.println("concrete method");
	}
}
 class B1 extends A1
{

	@Override
	public void m1() {
	System.out.println("implementation ");
		
	}
	
}
public class Demo1 
{
	 public static void main(String[] args) {
		B1 b = new B1();
		b.m1();
		b.m2();
	}

}
