package constructorCalls;

class Parent
{
	double radius=10.9;
		
}
class Child extends Parent
{
	double radius=20.2;
	
	public void area()
	{
		 System.out.println(super.radius*10);
	}
}

public class SuperDemo
{
	public static void main(String[] args) {
		Child c = new Child();
		c.area();
	}

}
