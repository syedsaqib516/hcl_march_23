package constructorCalls;

class X
{
	int x;
	String y;
	
	X()
	{
	
		System.out.println("no arg constructor");	
	}
	X(int x)
	{
		this();// should always be the first line inside a constructor body
		this.x=x;
		System.out.println("int arg constrcutor called");
	}
	X(int x , String y)
	{
		this(x);
		this.x=x;
		this.y=y;
		System.out.println("int String arg constrcutor called");
	}
}


public class ThisDemo 
{
	public static void main(String[] args) {
            X y = new X(10,"abcd");
	}
}
