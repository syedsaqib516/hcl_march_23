package constructorCalls;

//public class Object
//{
//	public Object
//	{
//		
//	}
//}
class Super
{
	Super(int x, String s)
	{
		super();
		System.out.println("Super class no arg constructor");
	}
}
class Sub extends Super
{
	Sub()
	{
		super(10,"abcd");
		System.out.println("Sub class no arg constructor");
	}
}

public class SuperMethodDemo {
	public static void main(String[] args) {
		new Sub();
	}	

}
