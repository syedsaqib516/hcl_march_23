package constructorCalls;

class Account
{
	long accNo;
	String IFCSCode;
	public Account(long accNo, String iFCSCode) {
		super();
		this.accNo = accNo;
		IFCSCode = iFCSCode;
	}
	
}
class SavingsAccount extends Account
{
	double rateOfInterest;
	public SavingsAccount(long accNo, String iFCSCode, double rateOfInterest, String loanApproval) {
		super(accNo, iFCSCode);
		this.rateOfInterest = rateOfInterest;
		this.loanApproval = loanApproval;
	}
	String loanApproval;
}
class CurrentAccount extends Account
{
	double transactionFee;
	public CurrentAccount(long accNo, String iFCSCode, double transactionFee, long transactionLimit) {
		super(accNo, iFCSCode);
		this.transactionFee = transactionFee;
		this.transactionLimit = transactionLimit;
	}
	long transactionLimit;
}


public class SuperMethodUSe 
{
	public static void main(String[] args) {
		new SavingsAccount(10000,"Fwt113",4.5,"not apporoved");
	}

}
