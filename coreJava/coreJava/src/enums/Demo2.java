package enums;
interface Itr
{
	String toString();
}
enum Brand implements  Itr
{
	LV(1899,"Louis Vuitton","Italy"),GUCCI(1878,"Gucci Le","Italy"),ARMANI(1889,"De Armani","Italy");
	int establishedIn;
    String founder;
    String headQuaters;
    
    public int getEstablishedIn() {
		return establishedIn;
	}
	public void setEstablishedIn(int establishedIn) {
		this.establishedIn = establishedIn;
	}
	public String getFounder() {
		return founder;
	}
	public void setFounder(String founder) {
		this.founder = founder;
	}
	public String getHeadQuaters() {
		return headQuaters;
	}
	public void setHeadQuaters(String headQuaters) {
		this.headQuaters = headQuaters;
	}

	
    Brand()
    {
    	
    }
	Brand( int establishedIn,String founder,String headQuaters)
	{
		this.establishedIn=establishedIn;
		this.founder=founder;
		this.headQuaters=headQuaters;
		
	}
	
	
	
}
public class Demo2
{
	public static void main(String[] args) 
	{
		Brand b[]=Brand.values();
		for(Brand brand:b)
		{
			System.out.println(brand+" "+brand.getEstablishedIn()+" "+brand.getFounder()+" "+brand.getHeadQuaters()+" "+brand.ordinal());
		}
	}

}
