package enums;

enum Colours
{	
	BLUE(10),BLACK(20),RED(30),GREEN(40),YELLOW(50),ORANAGE(50);
	
	int price;
	Colours(int price)
	{
		this.price=price;
		System.out.println("constructor executed");
	}
}



class Car
{
	public static final Car BMW=new Car();
	public static final Car AUDI=new Car();
	public static final Car MERC= new Car();
	public static final Car MUSTANG=new Car();
	public static final Car ROLLSROYCE=new Car();
	public static final Car FERRARI= new Car();
	public static final  Car LAMBORGHINI=new Car();
}

class Products
{
	
	
	private int productId;

	public Products() {
		super();
	}

  public Products(int productId, String productType, Colours colour, Brand brand, double price) {
		super();
		this.productId = productId;
		this.productType = productType;
		this.colour = colour;
		this.brand = brand;
		this.price = price;
	}
	private String productType;
	private Colours colour;
	private Brand brand;
	private double price;
	@Override
	public String toString() {
		return "Products [productId=" + productId + ", productType=" + productType + ", colour=" + colour + ", brand="
				+ brand + ", price=" + price + "]";
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public Colours getColour() {
		return colour;
	}
	public void setColour(Colours colour) {
		this.colour = colour;
	}
	public Brand getBrand() {
		return brand;
	}
	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}


}





public class EnumsDemo 
{
	public static void main(String[] args)
	{
		
		
		
		System.out.println(Colours.BLACK);
		System.out.println(Colours.BLACK.ordinal());
		System.out.println(Car.BMW);
		
		Products perfume = new Products(101,"Perfume",Colours.BLUE,Brand.GUCCI,100000);
		Products sunglasses = new Products(102,"Evidenve",Colours.BLACK,Brand.LV,120000);
		//Products shoes = new Products(103,"Pumps",Colours.RED,Brand.BALENCIAGA,140000);
		System.out.println(perfume);
		System.out.println(sunglasses);
//     /   System.out.println(shoes);
        
        Brand[] brands=Brand.values();
        for(Brand b:brands)
        {
        	System.out.println(b+" = "+b.ordinal());
        }
	}

}
