package enums;

public class EnumMain
{
	public static void main(String[] args) {
		System.out.println(DaysOfWeek.FRIDAY);
		
	   DaysOfWeek[] dow=DaysOfWeek.values();
	   
	   for(DaysOfWeek d:dow)
	   {
		   System.out.println(d+"  "+d.price+ "  "+d.ordinal());
	   }
	   
	   BankCustomer bc= new BankCustomer(9990909,"IFCS99088","cust@gmail","csju7y8",BankCustType.PLATINUM);
	   System.out.println(bc);
	}

}
