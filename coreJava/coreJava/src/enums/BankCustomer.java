package enums;

public class BankCustomer 
{
	long accNum;
	String ifcsCode;
	String password;
	String email;
	BankCustType custType;
	
	@Override
	public String toString() {
		return "BankCustomer [accNum=" + accNum + ", ifcsCode=" + ifcsCode + ", password=" + password + ", email="
				+ email + ", custType=" + custType + "]";
	}
	public long getAccNum() {
		return accNum;
	}
	public void setAccNum(long accNum) {
		this.accNum = accNum;
	}
	public String getIfcsCode() {
		return ifcsCode;
	}
	public void setIfcsCode(String ifcsCode) {
		this.ifcsCode = ifcsCode;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public BankCustType getCustType() {
		return custType;
	}
	public void setCustType(BankCustType custType) {
		this.custType = custType;
	}
	public BankCustomer(long accNum, String ifcsCode, String password, String email, BankCustType custType) {
		super();
		this.accNum = accNum;
		this.ifcsCode = ifcsCode;
		this.password = password;
		this.email = email;
		this.custType = custType;
	}


}
