package dateTimeApi;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;

public class Demo2
{
	public static void main(String[] args) {
		
		LocalDateTime dt= LocalDateTime.now();
		System.out.println(dt);
		
		LocalDateTime myBirthday =  LocalDateTime.of(1996,Month.APRIL,19,7,0);
		System.out.println(myBirthday);
		System.out.println("after 6 months "+myBirthday.plusMonths(6));
		System.out.println("before 6 months "+myBirthday.minusMonths(6));
		Period p = Period.between(LocalDate.of(1996,Month.APRIL,19), LocalDate.now());
		System.out.printf("age is %d years %d months and %d days", p.getYears(),p.getMonths(),p.getDays());
		
	
	}

}
