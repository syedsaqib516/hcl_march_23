package dateTimeApi;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;

//JODA time API joda.org 
public class Demo1 
{
	public static void main(String[] args) {
		LocalDate date =  LocalDate.now();
		System.out.println(date);
		
		LocalDate ld= LocalDate.of(1999,Month.SEPTEMBER, 19);

		int dd=date.getDayOfMonth();
		Month mm=date.getMonth();
		int yyyy= date.getYear();

		System.out.println(dd+"/"+mm+"/"+yyyy);
		System.out.printf("%d-%d-%d",mm,dd,yyyy);
		System.out.println();

		LocalTime lt = LocalTime.now();
		System.out.println(lt);

		int h=  lt.getHour();
		int m=  lt.getMinute();
		int s= lt.getSecond();
		int n= lt.getNano();
		System.out.printf("%d:%d:%d:%d",h,m,s,n);


	}

}
