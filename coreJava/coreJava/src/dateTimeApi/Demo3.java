package dateTimeApi;

import java.time.Year;
import java.util.Scanner;

public class Demo3 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter a year ");
		int yyyy=sc.nextInt();
		Year y = Year.of(yyyy);
		if(y.isLeap())
			System.out.printf("%d is a leap year",yyyy);
		else
			System.out.printf("%d is not a leap year",yyyy);
		
	}

}
