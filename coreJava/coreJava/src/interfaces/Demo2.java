package interfaces;

interface Dummy
{
	int add();
}

class SomeClass
{
	public int add()
	{
		System.out.println("some implementation");
		return 10;
	}
	
}

public class Demo2 extends SomeClass implements Dummy {
 
}
