package encapsulation;


public class Employee implements Comparable<Employee>// JAVA BEAN CLASS OR PLAIN OLD JAVA OBJECT CLASS (POJO)
{
	private  int empId =10;
	private  String empName ="saqib";
	private  double empSal =1.1;
		
	public Employee(int empId, String empName, double empSal) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empSal = empSal;
	}
	public Employee() {
		super();
		
	}

	public int getEmpId()// getter method
	{
		return empId;
	}
	
	public void setEmpId(int empId)//Setter method
	{
		this.empId=empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public double getEmpSal() {
		return empSal;
	}

	public void setEmpSal(double empSal) {
		this.empSal = empSal;
	}
	
	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", empSal=" + empSal + "]";
	}
	
	@Override
	public int compareTo(Employee o) {
		if(this.empId > (o.empId))
		{
			return 1;
		}
		else if (this.empId < o.empId)
		{
			return -1;
		}
		else
			return 0;
	}
}
