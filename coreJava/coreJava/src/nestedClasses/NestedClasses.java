package nestedClasses;

class A
{
	static class B
	{
		void m1()
		{
			System.out.println("inner method");
		}
		static void m2()
		{
			System.out.println("inner static method");
		}

		
	}
}

public class NestedClasses 
{
  public static void main(String[] args)
  {
	  A a =new A();
//	  A.B b= a.new B();// non static of innerclass
//	  b.m1();
	  A.B.m2();// static method of inner class
	
}
}
