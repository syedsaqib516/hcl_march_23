package nestedClasses;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class MapDemo 
{
	public static void main(String[] args)
	{
		HashMap<Integer,String> hm = new HashMap<Integer,String>();
		hm.put(1,"abcd");
		hm.put(2,"bcde");
		hm.put(3,"fghi");
		hm.put(4,"jklm");
		hm.put(5,"nopq");
		hm.put(1, "zyx");
		hm.put(12,null);
		hm.put(11,"avv");
		System.out.println(hm.get(null));
		System.out.println(hm.containsKey(1));
		System.out.println(hm.containsValue(null));
		hm.remove(1);
		
		System.out.println(hm);
		
		Set s1=hm.keySet();
		System.out.println(s1);
		Collection c1=hm.values();
		System.out.println(c1);
		
		Set<Entry<Integer,String>> s2=hm.entrySet();
		
		System.out.println(s2);
		
		Iterator<Entry<Integer,String>> i=s2.iterator();
		while(i.hasNext())
			
		{
			//System.out.println(i.next());
			Entry<Integer,String> e=i.next();
			System.out.println(e.getValue()+"  "+e.getKey());
		}
		
		
	}

}
