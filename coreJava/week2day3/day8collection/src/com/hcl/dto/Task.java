package com.hcl.dto;



public class Task implements Comparable<Task>{
	private static int totalTask=0;
	private int taskId;
	private String taskTitle;
	private String taskText;
	private String assignTo;
	private String clientName;
	private String startdate;
	private String submitDate;
	private String status;
	public Task(int taskId, String taskTitle, String taskText, String assignTo, String clientName, String startdate,
			String submitDate, String status) {
		super();
		this.taskId = taskId;
		this.taskTitle = taskTitle;
		this.taskText = taskText;
		this.assignTo = assignTo;
		this.clientName = clientName;
		this.startdate = startdate;
		this.submitDate = submitDate;
		this.status = status;
	}

	public static int getTotalTask() {
		return totalTask;
	}

	public static void setTotalTask(int totalTask) {
		Task.totalTask = totalTask;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public String getTaskText() {
		return taskText;
	}

	public void setTaskText(String taskText) {
		this.taskText = taskText;
	}

	public String getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Task [taskId=" + taskId + ", taskTitle=" + taskTitle + ", taskText=" + taskText + ", assignTo=" + assignTo
				+ ", clientName=" + clientName + ", startdate=" + startdate + ", submitDate=" + submitDate + ", status="
				+ status + "]";
	}

	public Task(int taskId, String taskTitle, String taskText, String clientName, String startdate) {
		super();
		this.taskId = taskId;
		this.taskTitle = taskTitle;
		this.taskText = taskText;
		this.clientName = clientName;
		this.startdate = startdate;
	}

	public Task() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compareTo(Task o) {
		// TODO Auto-generated method stub
		return o.getSubmitDate().compareTo(o.getSubmitDate());
	}

}
