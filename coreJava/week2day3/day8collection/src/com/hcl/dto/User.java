package com.hcl.dto;

public class User {
	public String email;
	public String pwd;
	public long mob;
	public String type;
	public User(String email, String pwd, String type) {
		super();
		this.email = email;
		this.pwd = pwd;
		this.type = type;
	}
	

	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPwd() {
		return pwd;
	}


	public void setPwd(String pwd) {
		this.pwd = pwd;
	}


	public long getMob() {
		return mob;
	}


	public void setMob(long mob) {
		this.mob = mob;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public User() {}

	@Override
	public String toString() {
		return "User [email=" + email + ", pwd=" + pwd + ", type=" + type + "]";
	}
}
