package com.hcl.exceptions;

public class NoElementForDeletionException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoElementForDeletionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
