package com.hcl.ui;

import java.util.List;
import java.util.Scanner;

import com.hcl.dao.TaskDaoImpl;
import com.hcl.dao.UserDaoImpl;
import com.hcl.dto.Task;
import com.hcl.dto.User;
import com.hcl.threads.ClientThread;
import com.hcl.threads.VisitorThread;

public class MainMenu {
	static UserDaoImpl ub = new UserDaoImpl();
	static TaskDaoImpl tob = new TaskDaoImpl();
	static Scanner sc = new Scanner(System.in);
    

	

	public static void main(String[] args) throws Exception {

		MainMenu m = new MainMenu();
		int ch;
		do {
			System.out.println("1- user registration.... plz enter email,pwd,type - v for visitor and c-for client");
			System.out.println("2- for login plz enter email and pwd......");
			System.out.println("3-exit..");
			ch = sc.nextInt();// 1,2,,

			switch (ch) {
			case 1:
				System.out.println("--to register--");
				String em = sc.next();
				String pwd = sc.next();
				String type = sc.next();
				User u = null;
				try {
				u = ub.signup(em, pwd, type);
				System.out.println("--detail:" + u);
				}catch (Exception e) {
					// TODO: handle exception
					System.err.println(e.getMessage());
				}
				break;

			case 2:

				System.out.println("-----TO Login--- email,pwd type-"); //
				String em1 = sc.next();
				String pwd1 = sc.next();
				User u1=null;
				try {
				u1 = ub.login(em1, pwd1);
				System.out.println(u1);
				}catch (Exception e) {
					// TODO: handle exception
					System.err.println(e.getMessage());
				}

				switch (u1.type) {

				case "c":
					
				     ClientThread ct=new ClientThread(u1);
				     ct.start();
				     ct.join();
					

					break;

				case "v":

					 VisitorThread vt=new VisitorThread(u1);
					 vt.start();
					 vt.join();
					break;
				default:
					break;
				}
				break;
			case 3:
				ch = 0;
				break;

			}

		} while (ch != 0);

	}
}
