package com.hcl.dao;

import java.util.ArrayList;

import java.util.List;

import com.hcl.dto.User;
import com.hcl.exceptions.InvalidCredentialsException;
import com.hcl.exceptions.SignUpValidateException;
import com.hcl.exceptions.ExistingUserException;

public class UserDaoImpl implements UserDao {
	static List<User> us = new ArrayList<User>();

	@Override
	public User login(String em, String pwd) {
		// TODO Auto-generated method stub
		int pos=0;
		User u=new User();
		if (us.isEmpty() == true) {
			System.out.println("sorry  no user in buffer");
		} else {
		for (int i = 0; i < us.size(); i++) {

			if ((us.get(i).getEmail().equals(em)) && (us.get(i).getPwd().equals(pwd))) {
				u = us.get(i);
				System.out.println("suucessfull login with" + us.get(i));
				pos++;
				break;
			}
		}
		if(pos==0) {
			throw new InvalidCredentialsException("You are not a vaid User...");
		}
		}

		return u;
	}

	@Override
	public User signup(String em, String pwd, String type) {
		// TODO Auto-generated method stub
		for (int i = 0; i < us.size(); i++) {
			if (us.get(i).getEmail().equals(em)) {
				throw new ExistingUserException("The user "+em+" is already existed..");
			}
		}
		if (!(em.contains("@hcl.com") && pwd.length() >= 5)) {
			throw new SignUpValidateException("The email or password invalid.(email must"
					+ " contains @hcl.com at the last and password size is greater than 5...");
		} 
		User u = new User(em, pwd, type);
		
        us.add(u);
		return u;
	}

	
	
	@Override
	public void showAllVisitor() {
		List<User> vs= new ArrayList<>();
		int pos = 0;
		for (int i = 0; i < us.size(); i++) {

			if (us.get(i).getType().equals("v")) {

				vs.add(us.get(i));

			}

		}

		System.out.println(vs);

	}

		// TODO Auto-generated method stub

	}


