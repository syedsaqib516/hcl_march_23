package com.hcl.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.hcl.dto.Task;
import com.hcl.exceptions.NoVisitorsForassignException;
import com.hcl.exceptions.NoElementForDeletionException;

public class TaskDaoImpl implements TaskDao ,Comparator<Task>{
	static List<Task> t1 = new ArrayList<>();

	@Override
	public Task addTask(Task t) {
		t1.add(t);
		return t;
	}

	@Override
	public int delTask(String taskname) {
		// TODO Auto-generated method stub
		int pos = 0;
		if (t1.isEmpty() != true ) {
			for (int i = 0; i < t1.size(); i++) {
				if (t1.get(i).getTaskTitle().equals(taskname) && t1.get(i).getAssignTo()==null) {
					t1.remove(i);
					pos++;
				}
			}
		} else {
			throw new NoElementForDeletionException("No Tasks for deletion...");
		}
		if (pos == 0) {
			System.out.println("Invalid Task name...");
			return 0;
		} else {
			return 1;
		}
	}
public void showall() {
	System.out.println(t1);
}
	@Override
	public Task update(String oldname, String newtaskname) {
		// TODO Auto-generated method stub
		Task tu = new Task();
		int pos = 0;
		if (t1.isEmpty() != true) {
			for (int i = 0; i < t1.size(); i++) {
				if (t1.get(i).getTaskTitle().equals(oldname)) {
					t1.get(i).setTaskTitle(newtaskname);
					tu = t1.get(i);
					pos++;
				}
			}
		} else {
			System.out.println("There is no tasks...");
		}
		if (pos > 0) {
			System.out.println("Invalid Task name...");
		}

		return tu;
	}

	@Override
	public List<Task> updateVisitorStatus(String taskname, String assignMail, String eDate) {
		List<Task> tu = new ArrayList<>();
		int pos = 0;
		if (t1.isEmpty() != true) {
			for (int i = 0; i < t1.size(); i++) {
				if (t1.get(i).getTaskTitle().equals(taskname)) {
					t1.get(i).setAssignTo(assignMail);
					t1.get(i).setSubmitDate(eDate);
					t1.get(i).setStatus("Completed");
					tu.add(t1.get(i));
					pos++;
				}
			}
		} else {
			System.out.println("There is no tasks...");
		}
		if (pos ==0) {
			System.out.println("Invalid Task name...");
		}

		return tu;
	}

	@Override
	public List<Task> incompleteTasks(String email) {
		// TODO Auto-generated method stub
		List<Task> t = new ArrayList<>();

		for (int i = 0; i < t1.size(); i++) {
			if (t1.get(i) != null && t1.get(i).getAssignTo() != null) {
				if (t1.get(i).getAssignTo().equals(email) && t1.get(i).getStatus() == null) {
					t.add(t1.get(i));
				}
			}

		}
		return t;
	}

	@Override
	public int findbyname(String serachByVisitor) {
		// TODO Auto-generated method stub
		int pos = -1;
		for (int i = 0; i < t1.size(); i++) {
			if (t1.get(i)!= null && t1.get(i).getTaskTitle().equals(serachByVisitor)) {
				pos=i;
				break;
			}

		}
		return pos;
	}

	@Override
	public int assignee(String updatebytaskname, String asigneename) {
		// TODO Auto-generated method stub
		int index = findbyname(updatebytaskname);
		if (index != -1) {
			t1.get(index).setAssignTo(asigneename);
			return 1;
		} else {
			throw new NoVisitorsForassignException("No visitors available for asigning task...");

		}
		
	}

	@Override
	public void ascByStartDate(List<Task> t) {
		// TODO Auto-generated method stub
		if(t!=null) {
      Collections.sort(t,new TaskDaoImpl());
		}

	}

	@Override
	public void ascByEndDate(List<Task> t) {
		// TODO Auto-generated method stub
		if(t!=null) {
		 Collections.sort(t);
		}
	}

	@Override
	public List<Task> completeTasksByAscEnddate(String email) {
		// TODO Auto-generated method stub
		List<Task> t=new ArrayList<>();
		
		for (int i = 0; i < t1.size(); i++) {
			if (t1.get(i) != null && t1.get(i).getAssignTo()!=null) {
				if (t1.get(i).getAssignTo().equals(email) && t1.get(i).getStatus() != null) {
					t.add(t1.get(i));
				}
			}

		}
		ascByEndDate(t);
		return t;
	}

		

	@Override
	public List<Task> showallByAscStartDate() {
		// TODO Auto-generated method stub
		List<Task> tmp =new ArrayList<>(t1);
		ascByStartDate(tmp);

		return tmp;
	}

	@Override
	public int compare(Task o1, Task o2) {
		// TODO Auto-generated method stub
		return o1.getStartdate().compareTo(o2.getStartdate());
	}

}
