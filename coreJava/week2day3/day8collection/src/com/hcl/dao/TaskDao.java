package com.hcl.dao;

import java.util.List;

import com.hcl.dto.Task;

public interface TaskDao{

	
	public Task addTask(Task t);
	public int delTask(String taskname);
	public Task update(String oldname,String newtaskname);
	public List<Task> updateVisitorStatus(String nwtaskname,String assignMail,String eDate);
	public List<Task> incompleteTasks(String email);
	public int findbyname(String serachByVisitor);
	public int assignee(String updatebytaskname, String asigneename);
	void ascByStartDate(List<Task> t);
	void ascByEndDate(List<Task> t);
	List<Task> completeTasksByAscEnddate(String email);
	List<Task> showallByAscStartDate();
	
}
