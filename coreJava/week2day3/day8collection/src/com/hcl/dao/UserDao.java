package com.hcl.dao;

import com.hcl.dto.User;

public interface UserDao {
	public User login(String em,String pwd);
	public User signup(String em,String pwd,String type);
	
	public void showAllVisitor();
}
