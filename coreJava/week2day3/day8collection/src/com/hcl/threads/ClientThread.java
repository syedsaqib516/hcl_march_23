package com.hcl.threads;

import java.util.List;
import java.util.Scanner;

import com.hcl.dao.TaskDaoImpl;
import com.hcl.dao.UserDaoImpl;
import com.hcl.dto.Task;
import com.hcl.dto.User;

public class ClientThread extends Thread {
	 UserDaoImpl ub = new UserDaoImpl();
	 TaskDaoImpl tob = new TaskDaoImpl();
	 Scanner sc = new Scanner(System.in);
     User u;
	

	public ClientThread(User u) {
		super();
		this.u = u;
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		menuforclient(u.getEmail());
	}


	public void menuforclient(String email) {

		int choice;

		do {

			System.out.println("1-Add Task");
			System.out.println("2-delete Task");
			System.out.println("3-update Task");
			System.out.println("4-assign Task");
			System.out.println("5-show all Task");
			System.out.println("6-show the all tasks by start date ascending order...");
			System.out.println("7-show the all tasks by start date Decending  order...");// problem
			System.out.println("8-exit...");
			choice = sc.nextInt();

			switch (choice) {
			case 1:
				System.out.println("task id ,taskname ,description,start date plz type..");
				System.out.println("please enter the task id..");
				int tid = sc.nextInt();
				System.out.println("please enter the task name..");
				String tname = sc.next();
				System.out.println("please enter the task dicription..");
				String desc = sc.next();
				System.out.println("please enter the task start date in the format of (dd-mm-yyyy)..");
				String sDate = sc.next();

				Task t1 = new Task(tid, tname, desc, email, sDate);
				try {
				Task t = tob.addTask(t1);
				System.out.println("inserted task:" + t);
				}catch (Exception e) {
					// TODO: handle exception
					e.getMessage();
				}

				break;

			case 2:
				System.out.println("2-plz type task name to delete...");
				String deltaskname = sc.next();
				try {
				tob.delTask(deltaskname);
				}catch (Exception e) {
					// TODO: handle exception
					e.getMessage();
				}
				break;

			case 3:

				System.out.println("3- plz insert sourcetask to update by new task name.");
				String oldtaskname = sc.next();
				String nwtaskname = sc.next();
                try {
				Task nwtask = tob.update(oldtaskname, nwtaskname);
				System.out.println(nwtask);
                }catch (Exception e) {
					// TODO: handle exception
                	e.getMessage();
				}

				break;

			case 4:
				System.out.println("---plz enter taskname and visitor name to assign");

				ub.showAllVisitor();
				String tkname = sc.next();
				String assignee = sc.next();
				try {
				int res = tob.assignee(tkname, assignee);
				System.out.println("task is updated with status:" + res);
				}catch (Exception e) {
					// TODO: handle exception
					e.getMessage();
				}
				break;

			case 5:
				System.out.println("--List Tasks--");
				tob.showall();
				break;
			case 6:
				System.out.println("List of tasks by start date");
				List<Task> tm = tob.showallByAscStartDate();
				System.out.println(tm);
				break;
			case 7:
				System.out.println("List of tasks by start date");
				List<Task> tt = tob.showallByAscStartDate();
				for (int i = tt.size() - 1; i >= 0; i--) {
					System.out.println(tt.get(i));
				}
				break;
			case 8:
				choice = 0;
				break;
			default:
				break;
			}

		} while (choice != 0);

	}

}
