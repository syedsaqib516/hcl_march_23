package com.hcl.threads;

import java.util.List;
import java.util.Scanner;

import com.hcl.dao.TaskDaoImpl;
import com.hcl.dao.UserDaoImpl;
import com.hcl.dto.Task;
import com.hcl.dto.User;

public class VisitorThread extends Thread{
	 UserDaoImpl ub = new UserDaoImpl();
     TaskDaoImpl tob = new TaskDaoImpl();
	 Scanner sc = new Scanner(System.in);
	 User u;
		

		public VisitorThread(User u) {
			// TODO Auto-generated constructor stub
			super();
			this.u = u;
		}
		
	@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
		}

	public void menuforvisitor(String email, String pwd) {

		int choice;

		do {

			System.out.println("1-show the incomplete tasks");// problem
			System.out.println("2-update the status");
			System.out.println("3-show the completed tasks by end date ascending order...");
			System.out.println("4-show the completed tasks by end date Decending  order...");
			System.out.println("5-Exit...");

			choice = sc.nextInt();

			switch (choice) {
			case 1:

				List<Task> st = tob.incompleteTasks(email);
				if (st != null) {
					System.out.println("Your tasks....");

					
					
							System.out.println(st);
						}else {
					System.out.println("There is no incomplete tasks for you...");
				}

				break;

			case 2:
				System.out.println("3- plz enter the task name and end date for updating status");
				String taskname = sc.next();
				System.out.println("Your updated tasks..");
				System.out.println("please enter the task end date in the format of (dd-mm-yyyy)..");
				String eDate = sc.next();
				List<Task> updatetTask = tob.updateVisitorStatus(taskname, email, eDate);
				if (updatetTask != null) {
					for (Task s1 : updatetTask) {
						
							System.out.println(s1);
						
					}
				} else {
					System.out.println("There is no task for you... ");
				}

				break;
			case 3:
				System.out.println("Your completed tasks......");
				List<Task> ss = tob.completeTasksByAscEnddate(email);
				if (ss != null) {
					System.out.println("Your tasks....");

					for (Task s1 : ss) {
						if (s1 != null) {
							System.out.println(s1);
						}
					}
				} else {
					System.out.println("There is no completed tasks for you...");
				}
				break;
			case 4:
				System.out.println("Your completed tasks......");
				List<Task> str = tob.completeTasksByAscEnddate(email);
				if (str != null) {
					System.out.println("Your tasks....");

					for (int i = str.size() - 1; i >= 0; i--) {
						if (str.get(i) != null) {
							System.out.println(str.get(i));
						}
					}

				} else {
					System.out.println("There is no completed tasks for you...");
				}
				break;
			case 5:
				choice = 0;
				break;
			default:
				break;

			}
		}while(choice!=0);

	}
}
