package demoApp.dao;

import java.util.ArrayList;

import demoApp.pojos.Task;

public class TaskDaoImp implements TaskDao {
	
	//Task[] tasks = new Task[5];
       ArrayList<Task> tasks = new ArrayList<Task>(10);

	@Override
	public void addTask(Task task) {	
           tasks.add(task);
	}

	@Override
	public void updateTask(int taskId, Task newTask) {
	    tasks.set(taskId, newTask);

	}

	@Override
	public void deleteTask(int taskId) {
		tasks.remove(taskId);
	}

	@Override
	public void viewAllTasks() {
		for(Task t:tasks)
		{
			System.out.println(t);
		}

	}

}
