package demoApp.dao;

import demoApp.pojos.User;

public interface UserDao 
{
	public void addUser(User user);
	public void loginUser(String userName,String Password);
	

}
