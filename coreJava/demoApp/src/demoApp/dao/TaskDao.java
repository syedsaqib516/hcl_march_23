package demoApp.dao;

import demoApp.pojos.Task;

public interface TaskDao
{
	public void addTask(Task t);
	public void updateTask(int taskId,Task newTask);
	public void deleteTask(int taskId);
	public void viewAllTasks();

}
