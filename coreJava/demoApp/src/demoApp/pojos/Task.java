package demoApp.pojos;

public class Task 
{
	private int taskId;
	

	public Task(int taskId) {
		super();
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "Task [taskId=" + taskId + "]";
	}
	
	

}
