package demo;

import java.io.IOException;

import jakarta.servlet.ServletException;


/**
 * Servlet implementation class Demo
 */
@jakarta.servlet.annotation.WebServlet("/Demo")
public class Demo extends jakarta.servlet.http.HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Demo() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(jakarta.servlet.http.HttpServletRequest request, jakarta.servlet.http.HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("hello world").append(request.getContextPath());
	}

}
