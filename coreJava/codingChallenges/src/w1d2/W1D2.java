package w1d2;

import java.util.Scanner;

public class W1D2 
{
	
	
	
	public static void main(String[] args)
	{
		
		Scanner scan = new Scanner(System.in);
		String[] todos= new String[5];
		int index=0;
		
		int choice = -1;
		do 
		{
			System.out.println("press 1 to add todo");
			System.out.println("press 2 to update todo");
			System.out.println("press 3 to delete todo");
			System.out.println("press 4 to search todo");
			System.out.println("press 5 to display all todos");
			System.out.println("press 0 to exit todo application");
			
			choice=scan.nextInt();
			scan.nextLine();
			
			switch(choice)
			{
			case 1:  System.out.println("enter todo to be added");
			        String todo=scan.nextLine();
			        todos[index++]=todo;
			         System.out.println(todo+" todo added successfully ");
			         
				break;
			case 2:  System.out.println("updating todo");
				break;
			case 3: System.out.println("deleting todo");
				break;
			case 4: System.out.println("searching todo");
				break;
			case 5: System.out.println("listing all todos");
			        for(String td: todos)
			        {
			        	System.out.println(td);
			        }
				break;
			default: System.out.println("please select a valid input");
			}
			
			
		}
		while(choice!=0);
		
	}

}
