package w1d3;

public class Todo 
{
	int taskId;
	String taskTitle;
	String taskText;
	String assignedTo;
	public Todo(int taskId, String taskTitle, String taskText, String assignedTo) {
		super();
		this.taskId = taskId;
		this.taskTitle = taskTitle;
		this.taskText = taskText;
		this.assignedTo = assignedTo;
	}
	@Override
	public String toString() {
		return "Todo [taskId=" + taskId + ", taskTitle=" + taskTitle + ", taskText=" + taskText + ", assignedTo="
				+ assignedTo + "]";
	}
	
}
