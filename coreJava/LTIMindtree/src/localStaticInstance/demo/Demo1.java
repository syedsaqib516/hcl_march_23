package localStaticInstance.demo;

class Employee
{
	int empId=10;  // non static / instance variable / object variables
	String empName="saqib";
	static String CompanyName="GL";// static
	
   Employee(int empId,String empName)
   {
	  this.empId=empId;
	  this.empName=empName;
   }
   
    void login(String username,String password)
    {
    	int x=10;// local are accessible only within the body they are declared in
    }
    
   static void logout()
   {
    	
   }
}






public class Demo1 
{
	
	public static void main(String[] args) {
		System.out.println(" main start");
		Employee emp1 = new Employee(10,"saqib");
		Employee emp2 = new Employee(20,"saqi");
		Employee emp3 = new Employee(30,"sai");
		
		Employee[] employees= {emp1,emp2,emp3};
		
		System.out.println("main end");
	}
	
	static {
		
		System.out.println(" static block executed");// data base connection
		
	}

}
