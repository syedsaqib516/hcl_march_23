package StringDemo;

public class Customer implements Comparable<Customer>
{
	private String custName;
	private String custEmail;
	private String custPhone;

	public Customer(String custName, String custEmail, String custPhone) {

		this.custName = custName;
		this.custEmail = custEmail;
		this.custPhone = custPhone;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustEmail() {
		return custEmail;
	}

	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	public String getCustPhone() {
		return custPhone;
	}
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	@Override
	public String toString() {
		return "Customer [custName=" + custName + ", custEmail=" + custEmail + ", custPhone=" + custPhone + "]";
	}
	@Override
	public int compareTo(Customer c)
	{
		if(this.getCustName().compareTo(c.getCustName())>1)
		{
			return 1;
		}
		else if(this.getCustName().compareTo(c.getCustName())<1)
		{
			return -1;
		}
		else
		{
			return 0;
		}

	}
}


