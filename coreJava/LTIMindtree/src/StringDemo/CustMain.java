package StringDemo;

import java.util.Arrays;

public class CustMain
{
	public static void main(String[] args) 
	{
		Customer[] customers= new Customer[5];
		customers[0]=new Customer("saqib","saqib@gmail","1234");
		customers[1]=new Customer("saqi","saqi@gmail","12341");
		customers[2]=new Customer("aqib","aqib@gmail","12342");
		customers[3]=new Customer("saib","saib@gmail","12343");
		customers[4]=new Customer("sam","sam@gmail","12344");
		
		Arrays.sort(customers,(c1,c2)->-c1.compareTo(c2));
		for(Customer c:customers)
		{
			System.out.println(c);
		}
		
	}

}
