package controlStatemments;

public class IfDemo 
{
	public static void main(String[] args) 
	{
		int age=16;
		
		if(age>=18)
		{
			System.out.println("allowed to vote");
		}
		else if(age>=60)
		{
			System.out.println("too old to vote");
		}
		else
		{
			System.out.println("not allowed to vote");
		}
		
		
		
	}

}
