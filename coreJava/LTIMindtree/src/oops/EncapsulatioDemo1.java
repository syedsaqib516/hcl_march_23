package oops;

class Customer
{
	private String custName;
	private String custEmail;
	private String custPhone;
	
	public Customer(String custName, String custEmail, String custPhone) {
		
		this.custName = custName;
		this.custEmail = custEmail;
		this.custPhone = custPhone;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustEmail() {
		return custEmail;
	}
	
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	public String getCustPhone() {
		return custPhone;
	}
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	@Override
	public String toString() {
		return "Customer [custName=" + custName + ", custEmail=" + custEmail + ", custPhone=" + custPhone + "]";
	}
}


public class EncapsulatioDemo1 
{
	public static void main(String[] args) 
	{
		String name="saqib";
		System.out.println(name.toString());
		System.out.println(name);
		Object o = new Object();
		System.out.println(o.toString());
		Customer c = new Customer("abc","def@gmail.com","934399876");
		System.out.println(c.toString());
		System.out.println(c.getCustName());
		c.setCustName("syed");
		System.out.println(c);
		
	}

}
