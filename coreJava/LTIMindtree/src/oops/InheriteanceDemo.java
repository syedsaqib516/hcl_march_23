package oops;
class Parent extends Object
{
	final double PI=3.14;
	static int x=10;
	
	//private members
	// constructors
	// blocks(static/nonstatic)
}

class Child extends Parent
{
}

public class InheriteanceDemo 
{
public static void main(String[] args) {
	Child c = new Child();
	System.out.println(c.PI);
	System.out.println(Child.x);
}
}
